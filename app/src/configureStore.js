import {
  compose,
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux';
import thunk from 'redux-thunk';

import * as authFeature from 'features/auth';
import * as filtersFeature from 'features/filters';
import { reducer as appReducer } from './modules/App/redux';

function configureStore(modules, api) {
  const middlewares = [
    thunk.withExtraArgument(api),
  ];

  const reducer = createReducer(modules);

  const store = createStore(
    reducer,
    compose(
      applyMiddleware(...middlewares),
      (process.env.ENV === 'browser' && window.devToolsExtension)
        ? window.devToolsExtension() : (arg => arg),
    ),
  );
  
  return store;
}

function createReducer(modules) {
  const reducersData = modules
        .filter(module => module.getReducer)
        .map(module => module.getReducer ? module.getReducer() : null);

  const modulesReducers = reducersData.reduce(
      (reducers, reducerData) => {
        return { ...reducers, [reducerData.name]: reducerData.reducer };
      },
      {},
  );
  return combineReducers({
    app: appReducer,
    filters: filtersFeature.reducer,
    auth: authFeature.reducer,
    ...modulesReducers,
  });
}

export { createReducer };
export default configureStore;
