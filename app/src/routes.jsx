import * as React from 'react';
import { Route, IndexRedirect } from 'react-router';
import App from './modules/App';

function createRoutes(modules) {
  return (
    <Route path="/">
      <Route path="admin" component={App}>
        { modules.map(module => module.getRoutes ? module.getRoutes() : null) }
      </Route>
      <IndexRedirect to="admin" />
    </Route>
  );
}

export default createRoutes;
