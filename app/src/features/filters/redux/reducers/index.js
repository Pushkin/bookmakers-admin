import { fromJS } from 'immutable';
import { actionTypes } from '../actions';
import initialState from '../initial';

function reducer(state = initialState, action) {
  const imState = fromJS(state);

  switch (action.type) {
    case actionTypes.UPDATE_FILTER_VALUES:
      return imState
        .setIn(['filterValues'], action.payload)
        .toJS();
      
    case actionTypes.RESET_FILTER_VALUES:
      return imState
        .setIn(['filterValues'], initialState.filterValues)
        .toJS();

    default:
      return imState.toJS();
  }
}

export default reducer;
