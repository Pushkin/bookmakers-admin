const actionTypes = {
  UPDATE_FILTER_VALUES: 'wj-admin/filters/UPDATE_FILTER_VALUES',
  RESET_FILTER_VALUES: 'wj-admin/filters/RESET_FILTER_VALUES',
};

function updateFilterValues(newFilterValues) {
  return { type: actionTypes.UPDATE_FILTER_VALUES, payload: newFilterValues };
}

function resetFilterValues(newFilterValues) {
  return { type: actionTypes.RESET_FILTER_VALUES };
}

export {
  actionTypes,
  updateFilterValues,
  resetFilterValues,
};
