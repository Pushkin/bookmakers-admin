import React, { Component, PropTypes } from 'react';
import block from 'bem-cn';
import { bind } from 'decko';
import { ControlLabel } from 'react-bootstrap';
import FieldGroup from 'shared/view/components/FieldGroup';
import './DateFilter.styl';

class DateFilter extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    label: PropTypes.string,
    value: PropTypes.shape({
      from: PropTypes.string,
      to: PropTypes.string,
    }),
    onChange: PropTypes.func,
  }

  static defaultProps = {
    label: '',
    value: {
      from: '',
      to: '',
    },
    onChange: () => {},
  }
  
  @bind
  onInputChange({ currentTarget: { id, value } }) {
    const { onChange, id: fieldID, value: fieldValue } = this.props;
    const newFieldValue = { ...fieldValue, [id]: value };
    if (value === '') delete newFieldValue[id];
    onChange(fieldID, newFieldValue);
  }

  render() {
    const b = block('date-filter');
    const { value, label, id } = this.props;
    return (
      <div className={b}>
        <ControlLabel className="input-label">
          { label !== '' ? label : id }
        </ControlLabel>
        <div className={b('inputs')}>
          <div className={b('input-wrapper')}>
            <span className={b('date-input-label')}>От</span>
            <FieldGroup onChange={this.onInputChange} id="from" type="date" value={value.from} />
          </div>
          <div className={b('input-wrapper')}>
            <span className={b('date-input-label')}>До</span>
            <FieldGroup onChange={this.onInputChange} id="to" type="date" value={value.to} />
          </div>
        </div>
      </div>
    );
  }
}

export default DateFilter;
