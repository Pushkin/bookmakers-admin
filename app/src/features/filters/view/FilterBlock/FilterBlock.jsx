import React, { Component, PropTypes } from 'react';
import { Panel, Button, Glyphicon } from 'react-bootstrap';
import { bind } from 'decko';
import block from 'bem-cn';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from '../../redux';
import FieldGroup from 'shared/view/components/FieldGroup';
import SelectGroup from 'shared/view/components/SelectGroup';
import CheckboxGroup from 'shared/view/components/CheckboxGroup';
import DateFilter from '../DateFilter';
import './FilterBlock.styl';

class FilterBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpened: false,
    };
  }

  static propTypes = {
    settings: PropTypes.shape({
      fields: PropTypes.object,
    }).isRequired,
    onApplyFilters: PropTypes.func.isRequired,
    filterValues: PropTypes.object.isRequired,
    updateFilterValues: PropTypes.func.isRequired,
  }

  @bind
  onHeaderClick() {
    this.setState({ isOpened: !this.state.isOpened });
  }

  @bind
  getFilteringFields(fields) {
    let filteringFields = [];
    if (fields) filteringFields = Object.keys(fields).filter(key => fields[key].filter);
    return filteringFields;
  }

  @bind
  onInputChange({ currentTarget: { id, value } }) {
    const { updateFilterValues, filterValues } = this.props;
    const newFilterValues = { ...filterValues, [id]: value };
    if (value === '') delete newFilterValues[id];
    updateFilterValues(newFilterValues);
  }

  @bind
  onSelectInputChange({ currentTarget: { id, value } }) {
    const { updateFilterValues, filterValues } = this.props;
    const newFilterValues = { ...filterValues, [id]: value };
    if (value === '') delete newFilterValues[id];
    updateFilterValues(newFilterValues);
  }

  @bind
  onDateInputChange(id, value) {
    const { updateFilterValues, filterValues } = this.props;
    const newFilterValues = { ...filterValues, [id]: value };
    if (!Object.keys(newFilterValues[id]).length) delete newFilterValues[id];
    updateFilterValues(newFilterValues);
  }
  
  @bind
  onApplyClick() {
    const { onApplyFilters, filterValues } = this.props;
    onApplyFilters(filterValues);
  }

  render() {
    const b = block('filter-block');
    const { isOpened } = this.state;
    const { settings, filterValues } = this.props;
    const panelHeader = (
      <div>
        <div onClick={this.onHeaderClick} className={b('panel-header')()}>
          <div className={b('panel-header-text')}>
            Фильтры
          </div>
          <Glyphicon glyph={isOpened ? 'chevron-up' : 'chevron-down'} />
        </div>
      </div>
    );
    const fields = settings ? settings.fields : null;
    const filteringFields = this.getFilteringFields(fields);
    const filtersList = filteringFields.map((fkey) => {
      const fieldName = fields[fkey].name ? fields[fkey].name : fkey;
      const filterValue = filterValues[fkey] ? filterValues[fkey] : '';
      let item = null;
      switch (fields[fkey].type) {
        case 'date':
          return <DateFilter onChange={this.onDateInputChange} id={fkey} value={filterValue} label={fieldName} />;

        case 'boolean':
          item = (
            <CheckboxGroup
              id={fkey}
              label={fieldName}
              onChange={this.onInputChange}
              value={filterValue}
            />
          );
          break;

        case 'enum':
          item = (
            <SelectGroup
              id={fkey}
              label={fieldName}
              itemsEnum={fields[fkey].enum}
              onChange={this.onSelectInputChange}
              value={filterValue}
              filtering
            />
          );
          break;

        default:
          item = (
            <FieldGroup
              key={fkey}
              id={fkey}
              type={fields[fkey].type}
              value={filterValue} 
              onChange={this.onInputChange}
              label={fieldName}
            />
          );
          break;
      }
      return <div className={b('filter-item-wrapper')()} >{item}</div>;
    });

    return (
      <div className={b}>
        <Panel className={b('filter-panel')()} bsStyle="primary" collapsible expanded={isOpened} header={panelHeader}>
          <div className={b('filters-container')}>{ filtersList }</div>
          <div className="submit-block">
            <Button bsStyle="success" onClick={this.onApplyClick}>Применить</Button>
          </div>
        </Panel>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    filterValues: state.filters.filterValues,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterBlock);
