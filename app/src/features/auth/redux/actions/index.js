import AuthConverter from '../data/converter';

const actionTypes = {
  SET_MODAL_VISIBILITY: 'wj-front/auth/SET_MODAL_VISIBILITY',
  ACTION_PROCESSING: 'wj-front/auth/ACTION_PROCESSING',
  SIGN_IN_SUCCESS: 'wj-front/auth/SIGN_IN_SUCCESS',
  ACTION_FAILURE: 'wj-front/auth/ACTION_FAILURE',
  SIGN_OUT: 'wj-front/auth/SIGN_OUT',
  LOAD_USER_INFO_SUCCESS: 'wj-front/auth/LOAD_USER_INFO_SUCCESS',
};

function setModalVisibility(isVisible) {
  return {
    type: actionTypes.SET_MODAL_VISIBILITY,
    payload: isVisible,
  };
}

function signIn(inputData) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'signIn',
      payload: inputData,
    };
    const response = await api.auth.signIn(requestData);
    
    if (response.success) {
      dispatch({ type: actionTypes.SIGN_IN_SUCCESS });
      api.setUserToken(response.data.token);
      dispatch({
        type: actionTypes.LOAD_USER_INFO_SUCCESS,
        payload: AuthConverter.convertUser(response.data.user),
      });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

function signOut() {
  return async (dispatch, getState, api) => {
    api.resetUserToken();
    dispatch({ type: actionTypes.SIGN_OUT });
  };
}

function checkUserIsAuthenticated() {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'checkAuth',
    };
    const response = await api.auth.checkAuth(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.SIGN_IN_SUCCESS });
      dispatch({ 
        type: actionTypes.LOAD_USER_INFO_SUCCESS,
        payload: AuthConverter.convertUser(response.data),
      });
    } else dispatch({ type: actionTypes.SIGN_OUT });
  };
}

export {
  actionTypes,
  setModalVisibility,
  signIn,
  signOut,
  checkUserIsAuthenticated,
};
