class AuthConverter {
  static convertUser(user) {
    return {
      ID: user.id,
      firstName: user.first_name,
      lastName: user.last_name,
    };
  }
}

export default AuthConverter;
