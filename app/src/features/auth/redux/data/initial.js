const initialState = {
  actionProcessing: false,
  errorMessage: null,
  isModalVisible: false,
  isAuthenticated: false,
  userInfo: {
    ID: -1,
    firstName: '',
    lastName: '',
  },
};

export default initialState;
