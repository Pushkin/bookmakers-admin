import { fromJS } from 'immutable';
import initialState from '../data/initial';
import { actionTypes } from '../actions';

function reducer(state = initialState, action) {
  const imState = fromJS(state);

  switch (action.type) {
    case actionTypes.SET_MODAL_VISIBILITY:
      return imState
        .setIn(['isModalVisible'], action.payload)
        .toJS();
    case actionTypes.ACTION_PROCESSING:
      return imState
        .setIn(['actionProcessing'], true)
        .toJS();
    case actionTypes.SIGN_IN_SUCCESS:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['isAuthenticated'], true)
        .setIn(['errorMessage'], null)
        .toJS();
    case actionTypes.ACTION_FAILURE:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['errorMessage'], action.payload)
        .toJS();
    case actionTypes.SIGN_OUT:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['isAuthenticated'], false)
        .setIn(['isModalVisible'], true)
        .toJS();
    case actionTypes.LOAD_USER_INFO_SUCCESS:
      return imState
        .setIn(['userInfo'], action.payload)
        .toJS();
    default:
      return imState
        .toJS();
  }
}

export default reducer;
