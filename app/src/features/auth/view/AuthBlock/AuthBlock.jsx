import React, { Component, PropTypes } from 'react';
import { bind } from 'decko';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import block from 'bem-cn';
import './AuthBlock.styl';
import LoginBlock from './LoginBlock';
import UserBlock from './UserBlock';
import { actions } from '../../redux';
import AuthModal from '../AuthModal';

class AuthBlock extends Component {
  static propTypes = {
    isModalVisible: PropTypes.bool.isRequired,
    setModalVisibility: PropTypes.func.isRequired,
    signIn: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired,
    signInProcessing: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    checkUserIsAuthenticated: PropTypes.func.isRequired,
    userInfo: PropTypes.shape({
      ID: PropTypes.number,
      firstName: PropTypes.string,
      lastName: PropTypes.string,
    }).isRequired,
  }

  @bind
  handleLogInButtonClick() {
    this.props.setModalVisibility(true);
  }

  @bind
  hideModal() {
    this.props.setModalVisibility(false);
  }

  @bind
  handleSignIn(inputData) {
    this.props.signIn(inputData);
  }

  @bind
  onSignInSuccess() {
    this.hideModal();
  }
  
  componentDidMount() {
    this.props.checkUserIsAuthenticated();
  } 

  render() {
    const { isModalVisible, signInProcessing, errorMessage, isAuthenticated } = this.props;
    const { userInfo, signOut } = this.props;
    const b = block('auth-block');
    return (
      <div className={b}>
        <AuthModal 
          onHideModal={this.hideModal}
          isModalVisible={isModalVisible}
          onSignInClick={this.handleSignIn}
          signInProcessing={signInProcessing}
          errorMessage={errorMessage}
          onSignInSuccess={this.onSignInSuccess}
          isAuthenticated={isAuthenticated}
        />
        { isAuthenticated ?
          <UserBlock userInfo={userInfo} onSignOut={signOut} />
          :
          <LoginBlock onLogInClick={this.handleLogInButtonClick} />
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isModalVisible: state.auth.isModalVisible,
    signInProcessing: state.auth.actionProcessing,
    errorMessage: state.auth.errorMessage,
    isAuthenticated: state.auth.isAuthenticated,
    userInfo: state.auth.userInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthBlock);
