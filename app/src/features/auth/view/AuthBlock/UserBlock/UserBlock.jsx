import React, { Component, PropTypes } from 'react';
import { Glyphicon } from 'react-bootstrap';
import block from 'bem-cn';
import HeaderButton from 'shared/view/elements/HeaderButton';
import './UserBlock.styl';

class UserBlock extends Component {
  static propTypes = {
    userInfo: PropTypes.shape({
      ID: PropTypes.number,
      firstName: PropTypes.string,
      lastName: PropTypes.string,
    }).isRequired,
    onSignOut: PropTypes.func.isRequired,
  }
  render() {
    const b = block('user-block');
    const { userInfo, onSignOut } = this.props;
    return (
      <div className={b}>

        <Glyphicon glyph="user" />
        <div className={b('user-info')}>{`${userInfo.firstName} ${userInfo.lastName}`}</div>
        <HeaderButton
          glyphName="log-out"
          onClick={onSignOut}
        />
      </div>
    );
  }
}

export default UserBlock;
