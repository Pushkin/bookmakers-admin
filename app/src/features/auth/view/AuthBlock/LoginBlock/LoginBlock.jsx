import React, { Component, PropTypes } from 'react';
import block from 'bem-cn';
import HeaderButton from 'shared/view/elements/HeaderButton';
import './LoginBlock.styl';

class LoginBlock extends Component {
  static propTypes = {
    onLogInClick: PropTypes.func.isRequired,
  }

  render() {
    const b = block('login-block');
    const { onLogInClick } = this.props;
    return (
      <div className={b}>
        <HeaderButton
          glyphName="log-in"
          onClick={onLogInClick}
          text="Вход"
        />
      </div>
    );
  }
}

export default LoginBlock;
