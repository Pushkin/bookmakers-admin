import React, { Component, PropTypes } from 'react';
import { Modal, Button } from 'react-bootstrap';
import block from 'bem-cn';
import { bind } from 'decko';
import FieldGroup from 'shared/view/components/FieldGroup';
import './AuthModal.styl';

class AuthModal extends Component {
  static propTypes = {
    isModalVisible: PropTypes.bool.isRequired,
    onHideModal: PropTypes.func.isRequired,
    onSignInClick: PropTypes.func.isRequired,
    signInProcessing: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    onSignInSuccess: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
  }

  @bind
  onInputChange({ currentTarget: { id, value } }) {
    this.setState({ [id]: value });
  }

  @bind
  handleSignInClick() {
    this.props.onSignInClick(this.state);
  }

  componentWillReceiveProps(nextProps) {
    const { signInProcessing, isAuthenticated, onSignInSuccess } = nextProps;
    if (this.props.signInProcessing && !signInProcessing && isAuthenticated) {
      onSignInSuccess();
    }
  }

  render() {
    const b = block('auth-modal');
    const { onHideModal, isModalVisible, errorMessage } = this.props;
    return (
      <Modal className={b()} show={isModalVisible} onHide={onHideModal}>
        <Modal.Header closeButton>
          <Modal.Title>Вход в систему</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FieldGroup
            id="email"
            label="E-mail"
            type="text"
            onChange={this.onInputChange}
          />
          <FieldGroup
            id="password"
            label="Пароль"
            type="password"
            onChange={this.onInputChange}
          />
          { errorMessage &&
            <div className="error-block">{errorMessage}</div>
          }
          <div className={b('submit-block')}>
            <Button className={b('submit-button')()} onClick={this.handleSignInClick}>Вход</Button>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

export default AuthModal;
