import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';

import createRoutes from './routes';
import configureStore, { createReducer } from './configureStore';
import './App.styl';
import Api from './shared/api/Api';

import { TopUpModule, CommonDataModule, TerminalCardsModule } from './modules';

let api;
if (process.env.NODE_ENV == 'production') {
  const loc = window.location;
  let newUri;
  if (loc.protocol === 'https:') {
    newUri = 'wss:';
  } else {
    newUri = 'ws:';
  }
  newUri += '//' + loc.host;
  newUri += '/admin/api/';
  api = new Api(newUri, onSocketOpened);
} else {
  api = new Api('ws://dc.wjbets.com/admin/api/', onSocketOpened);
}

const modules = [
  new TopUpModule(),
  new TerminalCardsModule(),
  new CommonDataModule(),
];
const store = configureStore(modules, api);
const routes = createRoutes(modules, store);
const rootComponent = (
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>
);

function onSocketOpened() {
  ReactDOM.render(rootComponent, document.querySelector('#root'));
}

export { ReactDOM };
export default rootComponent;
