import React, { Component, PropTypes } from 'react';
import { bind } from 'decko';
import block from 'bem-cn';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './AuthWrapper.styl';
import AuthBlock from 'features/auth';

class AuthWrapper extends Component {
  render() {
    const b = block('auth-wrapper');
    return (
      <div className={b}>
        <AuthBlock />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    
  };
}

function mapDispatchToProps(dispatch) {
  const actions = {

  };
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthWrapper);
