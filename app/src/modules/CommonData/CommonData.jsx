import * as React from 'react';
import { Route } from 'react-router';
import CommonDataListLayout from './view/ListLayout';
import CommonDataEditLayout from './view/EditLayout';
import CommonDataFullLayout from './view/FullLayout';
import { reducer } from './redux';

class CommonDataModule {
  getRoutes() {
    return (
      <Route key="common" path=":entity">
        <Route path="list" component={CommonDataListLayout} />
        <Route path="create" component={CommonDataEditLayout} />
        <Route path="edit/:id" component={CommonDataEditLayout} />
        <Route path="full/:id" component={CommonDataFullLayout} />
      </Route>
    );
  }

  getReducer() {
    return { name: 'commonData', reducer };
  }
}

export default CommonDataModule;
