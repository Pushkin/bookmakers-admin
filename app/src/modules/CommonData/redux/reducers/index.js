/*
ACTION_PROCESSING: 'wj-admin/common/ACTION_PROCESSING',
  ACTION_SUCCESS: 'wj-admin/common/ACTION_SUCCESS',
  ACTION_FAILURE: 'wj-admin/common/ACTION_FAILURE',
  LOAD_DATA_SUCCESS: 'wj-admin/common/LOAD_DATA_SUCCESS',
  LOAD_DATA_ITEM_SUCCESS: 'wj-admin/common/LOAD_DATA_ITEM_SUCCESS',
  FLUSH_REQUEST_DATA: 'wj-admin/common/FLUSH_REQUEST_DATA',
*/

import { fromJS } from 'immutable';
import initialState from '../initial';
import { actionTypes } from '../actions';

function reducer(state = initialState, action) {
  const imState = fromJS(state);

  switch (action.type) {

    case actionTypes.ACTION_PROCESSING:
      return imState
        .setIn(['actionProcessing'], true)
        .setIn(['actionSuccess'], false)
        .setIn(['errorMessage'], null)
        .setIn(['loadSettingsSuccess'], false)
        .setIn(['loadDataItemSuccess'], false)
        .toJS();

    case actionTypes.ACTION_FAILURE:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['actionSuccess'], false)
        .setIn(['errorMessage'], action.payload)
        .toJS();

    case actionTypes.ACTION_SUCCESS:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['actionSuccess'], true)
        .setIn(['errorMessage'], null)
        .toJS();
    
    case actionTypes.FLUSH_REQUEST_DATA:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['actionSuccess'], false)
        .setIn(['errorMessage'], null)
        .toJS();

    case actionTypes.LOAD_SETTINGS_SUCCESS:
      return imState
        .setIn(['loadSettingsSuccess'], true)
        .setIn(['actionProcessing'], false)
        .setIn(['errorMessage'], null)
        .setIn(['settings'], action.payload)
        .toJS();

    case actionTypes.LOAD_DATA_SUCCESS:
      return imState
        .setIn(['data'], action.payload.items)
        .setIn(['totalItemsCount'], action.payload.count)
        .toJS();

    case actionTypes.LOAD_DATA_FAILURE:
      return imState
        .setIn(['data'], [])
        .setIn(['actionProcessing'], false)
        .setIn(['actionSuccess'], false)
        .setIn(['errorMessage'], action.payload)
        .toJS();
    
    case actionTypes.LOAD_DATA_ITEM_SUCCESS:
      return imState
        .setIn(['activeDataItem'], action.payload)
        .setIn(['loadDataItemSuccess'], true)
        .setIn(['actionProcessing'], false)
        .setIn(['errorMessage'], null)
        .toJS();
    
    case actionTypes.SET_ACTIVE_PAGE:
      return imState
        .setIn(['activePage'], action.payload)
        .toJS();

    case actionTypes.UPDATE_DISPLAY_PARAMS:
      return imState
        .setIn(['displayParams'], action.payload)
        .toJS();

    case actionTypes.FLUSH_DISPLAY_PARAMS:
      return imState
        .setIn(['displayParams'], {
          ...initialState.displayParams,
          count: action.payload })
        .toJS();

    default:
      return imState
        .toJS();
  }
}

export default reducer;
