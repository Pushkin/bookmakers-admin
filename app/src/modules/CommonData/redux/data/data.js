const optionsPageLimit = {
  ten: { value: 10, text: 10 },
  fifteen: { value: 15, text: 15 },
  thirty: { value: 30, text: 30 },
  fifty: { value: 50, text: 50 },
  hundred: { value: 100, text: 100 },
  all: { value: NaN, text: 'Все' },
};

export { optionsPageLimit };
