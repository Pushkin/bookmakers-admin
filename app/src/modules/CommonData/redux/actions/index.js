const actionTypes = {
  ACTION_PROCESSING: 'wj-admin/commonData/ACTION_PROCESSING',
  ACTION_SUCCESS: 'wj-admin/commonData/ACTION_SUCCESS',
  ACTION_FAILURE: 'wj-admin/commonData/ACTION_FAILURE',
  LOAD_SETTINGS_SUCCESS: 'wj-admin/commonData/LOAD_SETTINGS_SUCCESS',
  LOAD_DATA_SUCCESS: 'wj-admin/commonData/LOAD_DATA_SUCCESS',
  LOAD_DATA_FAILURE: 'wj-admin/commonData/LOAD_DATA_FAILURE',
  LOAD_DATA_ITEM_SUCCESS: 'wj-admin/commonData/LOAD_DATA_ITEM_SUCCESS',
  FLUSH_REQUEST_DATA: 'wj-admin/commonData/FLUSH_REQUEST_DATA',
  FLUSH_DISPLAY_PARAMS: 'wj-admin/commonData/FLUSH_DISPLAY_PARAMS',
  UPDATE_DISPLAY_PARAMS: 'wj-admin/commonData/UPDATE_DISPLAY_PARAMS',
  SET_ACTIVE_PAGE: 'wj-admin/commonData/SET_ACTIVE_PAGE',
  UPDATE_HTML_IMG: 'wj-admin/commonData/UPDATE_HTML_IMG',
};

function loadSettings(entityName) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'loadSettings',
      params: {
        entityName,
      },
    };
    const response = await api.common.loadSettings(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.LOAD_SETTINGS_SUCCESS, payload: response.data });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

function loadData(entityName, params) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'loadData',
      params: {
        entityName,
        args: {
          ...params,
        },
      },
    };
    const response = await api.common.loadData(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.ACTION_SUCCESS });
      dispatch({ type: actionTypes.LOAD_DATA_SUCCESS, payload: response.data });
    } else dispatch({ type: actionTypes.LOAD_DATA_FAILURE, payload: response.errorMessage });
  };
}

function loadDataItem(entityName, itemID) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'loadDataItem',
      params: {
        entityName,
        itemID,
      },
    };
    const response = await api.common.loadDataItem(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.LOAD_DATA_ITEM_SUCCESS, payload: response.data });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

function createDataItem(entityName, data) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'createDataItem',
      params: {
        entityName,
      },
      data,
    };
    const response = await api.common.createDataItem(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.ACTION_SUCCESS });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

function editDataItem(entityName, itemID, data) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'editDataItem',
      params: {
        entityName,
        itemID,
      },
      data,
    };
    const response = await api.common.editDataItem(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.ACTION_SUCCESS });
      dispatch({ type: actionTypes.LOAD_DATA_ITEM_SUCCESS, payload: response.data });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

function removeDataItem(itemID, entityName) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'removeDataItem',
      params: {
        itemID,
        entityName,
      },
    };
    const response = await api.common.removeDataItem(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.ACTION_SUCCESS });
      dispatch({ type: actionTypes.LOAD_DATA_SUCCESS, payload: response.data });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

function flushRequestData() {
  return { type: actionTypes.FLUSH_REQUEST_DATA };
}

function flushDisplayParams(countPage) {
  return { type: actionTypes.FLUSH_DISPLAY_PARAMS, payload: countPage };
}

function updateDisplayParams(newDisplayParams) {
  return {
    type: actionTypes.UPDATE_DISPLAY_PARAMS,
    payload: newDisplayParams,
  };
}

function setActivePage(activePage) {
  return {
    type: actionTypes.SET_ACTIVE_PAGE,
    payload: activePage,
  };
}

function updateHtmlImg(dataItem, html, key) {
  function getExtentionInBase64(src) {
    const arr = src.split(',');
    const extRegEx = /(jpeg|png|jpg|gif)/;
    const ext = extRegEx.exec(arr[0]);
    if (!ext) return;
    const extension = ext[0];
    const code = arr[1];
    return { extension, code };
  }

  return async (dispatch, getState, api) => {
    const newDiv = document.createElement('div');
    newDiv.innerHTML = html;
    const images = newDiv.querySelectorAll('img');
    const base64regex = /(base64)/;
    for (const img of images) {
      if (base64regex.test(img.src)) {
        const base64 = img.src;
        const { extension, code } = getExtentionInBase64(base64);
        if (extension && code) {
          dispatch({ type: actionTypes.ACTION_PROCESSING });
          console.log(extension, code);
          const requestData = {
            key: 'uploadImage',
            data: { image: code, ext: extension },
          };
          const response = await api.common.uploadImage(requestData);
          if (response.success) {
            img.src = response.data.url;
            const updatedDataItem = {
              ...dataItem,
              [key]: newDiv.innerHTML,
            };
            dispatch({ type: actionTypes.LOAD_DATA_ITEM_SUCCESS, payload: updatedDataItem });
          } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
        }
      }
    }
  };
}

export {
  actionTypes,
  loadSettings,
  loadData,
  loadDataItem,
  createDataItem,
  editDataItem,
  removeDataItem,
  flushRequestData,
  updateDisplayParams,
  setActivePage,
  flushDisplayParams,
  updateHtmlImg,
};
