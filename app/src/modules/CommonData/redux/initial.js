const initialState = {
  actionProcessing: false,
  errorMessage: null,
  actionSuccess: false,
  data: [],
  totalItemsCount: 0,
  displayParams: {
    order_by: '-id',
    offset: 0,
    count: 30,
    filters: {},
  },
  activePage: 1,
  activeDataItem: null,
  settings: null,
  loadSettingsSuccess: false,
  loadDataItemSuccess: false,
};

export default initialState;
