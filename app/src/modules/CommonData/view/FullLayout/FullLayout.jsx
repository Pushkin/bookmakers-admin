import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { bind } from 'decko';
import { Table } from 'react-bootstrap';
import block from 'bem-cn';
import Spinner from 'shared/view/components/Spinner';
import { actions } from '../../redux';
import { convertDate } from 'shared/utils/data';
import { getEntityName } from 'shared/utils/data';
import './FullLayout.styl';

class CommonDataFullLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    loadSettings: PropTypes.func.isRequired,
    loadDataItem: PropTypes.func.isRequired,

    actionProcessing: PropTypes.bool.isRequired,
    loadSettingsSuccess: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,

    settings: PropTypes.object.isRequired,
    activeDataItem: PropTypes.object.isRequired,
  }

  componentDidMount() {
    const { loadSettings, params } = this.props;
    loadSettings(params.entity);
  }

  componentWillReceiveProps(nextProps) {
    const {
      actionProcessing,
      loadSettingsSuccess,
    } = nextProps;

    if (this.props.actionProcessing && !actionProcessing) {
      if (loadSettingsSuccess) this.onLoadSettingsSuccess();
    }
  }
  
  @bind
  onLoadSettingsSuccess() {
    const { loadDataItem, params } = this.props;
    if (params.id) loadDataItem(params.entity, params.id);
  }

  render() {
    const { settings, actionProcessing, errorMessage, activeDataItem, params } = this.props;
    const fields = settings ? settings.fields : [];
    let fieldsList = null;
    const b = block('full-data-layout');
    if (activeDataItem != null) {
      fieldsList = Object.keys(fields).map((key) => {
        let elem = null;
        switch (fields[key].type) {
          case 'boolean':
            elem = <td>{ activeDataItem[key] ? 'Да' : 'Нет' }</td>;
            break;
          case 'date':
            elem = <td>{ convertDate(activeDataItem[key]) }</td>;
            break;
          case 'enum':
            const enumElem = fields[key].enum[activeDataItem[key]];
            elem = <td>{ enumElem }</td>;
            break;
          default:
            elem = <td>{ activeDataItem[key] }</td>;
            break;
        }
        return (
          <tr>
            <td>{fields[key].name ? fields[key].name : key}:</td>
            { elem }
          </tr>
        );
      });
    }

    return (
      <div className={b}>
        <div className="center-content-block">
          <h3>{`${getEntityName(params.entity)}: Просмотр (ID ${params.id})`}</h3>
        </div>
        <div className={b('content')}>
          <Spinner isLoading={actionProcessing} />
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>Имя поля</th>
                <th>Значение</th>
              </tr>
            </thead>
            <tbody>
              { !actionProcessing && fieldsList }
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    actionProcessing: state.commonData.actionProcessing,
    loadSettingsSuccess: state.commonData.loadSettingsSuccess,
    errorMessage: state.commonData.errorMessage,
    settings: state.commonData.settings,
    activeDataItem: state.commonData.activeDataItem,
    actionSuccess: state.commonData.actionSuccess,
    loadDataItemSuccess: state.commonData.loadDataItemSuccess,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CommonDataFullLayout);
