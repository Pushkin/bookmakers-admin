import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { bind } from 'decko';
import { Button } from 'react-bootstrap';
import FieldGroup from 'shared/view/components/FieldGroup';
import SubmitBlock from 'shared/view/components/SubmitBlock';
import DatePickerGroup from 'shared/view/components/DatePickerGroup';
import FileGroup from 'shared/view/components/FileGroup';
import CheckboxGroup from 'shared/view/components/CheckboxGroup';
import SelectGroup from 'shared/view/components/SelectGroup';
import HtmlGroup from 'shared/view/components/HtmlGroup';
import { getEntityName } from 'shared/utils/data';
import { actions } from '../../redux';
import './EditLayout.styl';
import readFileAsBase64 from 'shared/utils/fileToBase64';

class CommonDataEditLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    loadSettings: PropTypes.func.isRequired,
    loadDataItem: PropTypes.func.isRequired,
    createDataItem: PropTypes.func.isRequired,
    editDataItem: PropTypes.func.isRequired,
    flushRequestData: PropTypes.func.isRequired,
    updateHtmlImg: PropTypes.func.isRequired,

    actionProcessing: PropTypes.bool.isRequired,
    loadSettingsSuccess: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    actionSuccess: PropTypes.bool.isRequired,
    loadDataItemSuccess: PropTypes.bool.isRequired,

    settings: PropTypes.object.isRequired,
    activeDataItem: PropTypes.object.isRequired,
  }

  componentDidMount() {
    const { loadSettings, params, flushRequestData } = this.props;
    loadSettings(params.entity);
  }

  componentWillReceiveProps(nextProps) {
    const {
      actionProcessing,
      loadSettingsSuccess,
      loadDataItemSuccess,
      activeDataItem,
    } = nextProps;
    if (this.props.actionProcessing && !actionProcessing) {
      if (loadSettingsSuccess) this.onLoadSettingsSuccess();
      if (loadDataItemSuccess) this.onLoadDataItemSuccess(activeDataItem);
    }
  }
  
  @bind
  onLoadSettingsSuccess() {
    const { loadDataItem, params } = this.props;
    if (params.id) loadDataItem(params.entity, params.id);
  }
  @bind
  setDefaultValue(fields, fieldsList){
    fields.map((key) => {
      if (fieldsList[key].type === 'boolean' && this.state[key] === undefined ) {
        const value = fieldsList[key].is_checked ? fieldsList[key].is_checked : false;
        this.setState({ [key]: value });
      }
    });
  }

  @bind
  onLoadDataItemSuccess(activeDataItem) {
    this.fillStateWithEditableValues(activeDataItem);
  }

  @bind
  fillStateWithEditableValues(activeDataItem) {
    const { settings } = this.props;
    const fields = settings ? settings.fields : null;
    const editableFields = this.getEditableFields(fields);
    const fieldsToState = editableFields.reduce((res, cur) => {
      return {
        ...res,
        [cur]: activeDataItem[cur],
      };
    }, {});
    this.setState((_state) => ({ ..._state, ...fieldsToState }));
  }

  @bind
  getEditableFields(fields) {
    let editableFields = [];
    if (fields) editableFields = Object.keys(fields).filter(key => fields[key].editable);
    return editableFields;
  }

  @bind
  onHtmlInputChange(key, html) {
    this.setState({ [key]: html });
    const { updateHtmlImg, activeDataItem } = this.props;
    updateHtmlImg(activeDataItem, html, key);
  }

  @bind
  onInputChange({ currentTarget: { id, value } }) {
    console.log(`${id}: ${value}`);
    this.setState({ [id]: value });
  }

  @bind
  onCheckBoxChange(currentTarget) {
    const key = currentTarget.target.id;
    console.log(`${currentTarget.target.id}: ${!this.state[key]}`);
    this.setState({ [key]: !this.state[key] });
  }

  @bind
  onImgChange({currentTarget: { id, files }}) {
    const img = files[0]
    const result_img = readFileAsBase64(img)
    result_img.then((result) => {
      const arr = result.split(',');
      const extRegEx = /(jpeg|png|jpg|gif)/;
      const ext = extRegEx.exec(arr[0]);
      if (!ext) return;
      const extension = ext[0];
      const code = arr[1];
      this.setState({ 'ext': extension});
      this.setState({ 'image' : code});
    })
  }


  @bind
  onFileChange({currentTarget: { id, files }}) {
    const { fileToUpload } = this.state;
    const file = files[0]
    const resultFile = readFileAsBase64(file)
    resultFile.then((result) => {
      const arr = result.split(',');
      const extRegEx = /(jpeg|png|jpg|gif|zip|rar)/;
      const ext = extRegEx.exec(arr[0]);
      if (!ext) return;
      const extension = ext[0];
      const code = arr[1];
      const filanyFile = { ...fileToUpload };
      filanyFile[id] = {
        ext: extension,
        file: code,
      };
      this.setState({ fileToUpload: filanyFile });
    });
  }

  @bind
  createPrimitiveOnInputChange(name) {
    return (value) => {
      const parsed = new Date(value).toISOString();
      const dateEndIndex = parsed.indexOf('.');
      const final = parsed.slice(0, dateEndIndex).replace('T', ' ');
      this.setState({ [name]: final });
    }
  }

  @bind
  onSaveClick() {
    const { createDataItem, editDataItem, params } = this.props;
    if (params.id) {
      editDataItem(params.entity, params.id, this.state);
    } else createDataItem(params.entity, this.state);
  }

  render() {
    const { settings, actionProcessing, errorMessage, actionSuccess, params } = this.props;
    const fields = settings ? settings.fields : null;
    const editableFields = this.getEditableFields(fields);
    if (editableFields) this.setDefaultValue(editableFields, fields);
    const fieldsList = editableFields.map((key) => {
      const fieldName = fields[key].name ? fields[key].name : key;
      let fieldElem = null;
      switch (fields[key].type) {
        case 'textarea':
          fieldElem = (
            <FieldGroup
              id={key}
              label={fieldName}
              componentClass="textarea"
              onChange={this.onInputChange}
              rows={20}
              value={this.state[key]}
            />
          );
          break;

        case 'text':
          fieldElem = (
            <FieldGroup
              id={key}
              label={fieldName}
              type="text"
              onChange={this.onInputChange}
              value={this.state[key]}
            />
          );
          break;

        case 'number':
          fieldElem = (
            <FieldGroup
              id={key}
              label={fieldName}
              type="number"
              onChange={this.onInputChange}
              value={this.state[key]}
            />
          );
          break;
        
        case 'date':
          fieldElem = (
            <DatePickerGroup
              id={key}
              label={fieldName}
              onChange={this.createPrimitiveOnInputChange(key)}
              value={this.state[key]}
            />
          );
          break;
        
        case 'boolean':
          fieldElem = (
            <CheckboxGroup
              id={key}
              label={fieldName}
              onChange={this.onCheckBoxChange}
              checked={this.state[key]}
            />
          );
          break;

        case 'html':
          fieldElem = (
            <HtmlGroup
              id={key}
              label={fieldName}
              onChange={html => this.onHtmlInputChange(key, html)}
              value={this.state[key]}
            />
          );
          break;
        
        case 'enum':
          fieldElem = (
            <SelectGroup
              id={key}
              label={fieldName}
              itemsEnum={fields[key].enum}
              onChange={this.onInputChange}
              value={this.state[key]}
            />
          );
          break;

        case 'img':
          fieldElem = (
              this.state[key] ?
                <img className="edit-layout__img" src={this.state[key]} alt="" />
                  :
                (<FileGroup
                  id={key}
                  label={fieldName}
                  onChange={this.onImgChange}
                  value={this.state[key]}
                  />)
          );
          break;

        case 'file':
          fieldElem = (
              this.state[key] ?
                <img className="edit-layout__img" src={this.state[key]} alt="" />
                  :
                (<FileGroup
                  id={key}
                  label={fieldName}
                  onChange={this.onFileChange}
                  value={this.state[key]}
                  />)
          );
          break;

        default:
          break;
      }
      return fieldElem;
    });

    return (
      <div>
        <div className="center-content-block">
          <h3>
            {`${getEntityName(params.entity)}: `}
            { params.id ? `Редактирование (ID ${params.id})` : 'Создание'}
          </h3>
        </div>
        { fieldsList }

        <SubmitBlock
          submitText="Сохранить"
          actionProcessing={actionProcessing}
          actionSuccess={actionSuccess}
          errorMessage={errorMessage}
          onSubmitClick={this.onSaveClick}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    actionProcessing: state.commonData.actionProcessing,
    loadSettingsSuccess: state.commonData.loadSettingsSuccess,
    errorMessage: state.commonData.errorMessage,
    settings: state.commonData.settings,
    activeDataItem: state.commonData.activeDataItem,
    actionSuccess: state.commonData.actionSuccess,
    loadDataItemSuccess: state.commonData.loadDataItemSuccess,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CommonDataEditLayout);
