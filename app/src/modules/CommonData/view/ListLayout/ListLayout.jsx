import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bind } from 'decko';
import { Button, Pagination } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import block from 'bem-cn';
import { actions } from '../../redux';
import DataTableView from 'shared/view/components/DataTableView';
import FilterBlock from 'features/filters';
import { actions as filterActions } from 'features/filters';
import { getEntityName, PermissionTypes } from 'shared/utils/data';
import { optionsPageLimit } from '../../redux/data/data.js';
import './ListLayout.styl';

class CommonDataListLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countPage: 30,
    };
  }

  static propTypes = {
    settings: PropTypes.object.isRequired,
    data: PropTypes.arrayOf(Object).isRequired,
    totalItemsCount: PropTypes.number.isRequired,
    actionProcessing: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    removeDataItem: PropTypes.func.isRequired,
    loadData: PropTypes.func.isRequired,
    loadSettings: PropTypes.func.isRequired,
    loadSettingsSuccess: PropTypes.bool.isRequired,
    updateDisplayParams: PropTypes.func.isRequired,
    setActivePage: PropTypes.func.isRequired,
    displayParams: PropTypes.shape({
      offset: PropTypes.number,
      count: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]),
      order_by: PropTypes.string,
    }).isRequired,
    activePage: PropTypes.number.isRequired,
    flushDisplayParams: PropTypes.func.isRequired,
    resetFilterValues: PropTypes.func.isRequired,
  }

  static contextTypes = {
    router: PropTypes.object,
  }

  componentDidMount() {
    const { loadSettings, params } = this.props;
    loadSettings(params.entity);
  }
  
  componentWillReceiveProps(nextProps) {
    const {
      actionProcessing,
      loadSettingsSuccess,
      params,
      loadSettings,
      flushDisplayParams,
      setActivePage,
      resetFilterValues,
    } = nextProps;
    const { countPage } = this.state;
    if (this.props.actionProcessing && !actionProcessing && loadSettingsSuccess) {
      this.onLoadSettingsSuccess();
    }

    if (params.entity !== this.props.params.entity) {
      loadSettings(params.entity);
      flushDisplayParams(countPage);
      resetFilterValues();
      setActivePage(1);
    }
  }

  @bind
  onCreateDataItemClick() {
    const { params } = this.props;
    this.context.router.push(`/admin/${params.entity}/create`);
  }

  @bind
  onLoadSettingsSuccess() {
    const { loadData, params, displayParams } = this.props;
    loadData(params.entity, displayParams);
  }
  
  @bind
  onShowFullDataItemClick(item) {
    const { params } = this.props;
    this.context.router.push(`/admin/${params.entity}/full/${item}`);
  }

  @bind
  onDataItemEditClick(item) {
    const { params } = this.props;
    this.context.router.push(`/admin/${params.entity}/edit/${item}`);
  }

  @bind
  onRemoveDataItemClick(itemID) {
    const { removeDataItem, params } = this.props;
    removeDataItem(itemID, params.entity);    
  }

  @bind
  onFieldClick(fieldName) {
    const { loadData, params, displayParams, updateDisplayParams, setActivePage } = this.props;
    const newDisplayParams = {
      ...displayParams,
      order_by: fieldName,
      offset: 0,
    };
    updateDisplayParams(newDisplayParams);
    setActivePage(1);
    loadData(params.entity, newDisplayParams);
  }

  @bind
  handlePageSelect(page) {
    const { loadData, params, displayParams, updateDisplayParams, setActivePage } = this.props;
    const newDisplayParams = {
      ...displayParams,
      offset: displayParams.count * (page - 1),
    };
    updateDisplayParams(newDisplayParams);
    setActivePage(page);
    loadData(params.entity, newDisplayParams);
  }

  @bind
  onApplyFilters(filters) {
    const { loadData, params, displayParams, updateDisplayParams, setActivePage } = this.props;
    const newDisplayParams = {
      ...displayParams,
      offset: 0,
      filters,
    };
    updateDisplayParams(newDisplayParams);
    setActivePage(1);
    loadData(params.entity, newDisplayParams);
  }

  @bind
  onChangeCountPage(e) {
    const { loadData, params, displayParams, updateDisplayParams, setActivePage } = this.props;
    const newCount = e.currentTarget.value;
    this.setState({ countPage: +newCount });
    const newDisplayParams = {
      ...displayParams,
      count: +newCount,
    };
    updateDisplayParams(newDisplayParams);
    setActivePage(1);
    loadData(params.entity, newDisplayParams);
  }

  render() {
    const {
      data, settings, actionProcessing, totalItemsCount, displayParams,
      params, activePage,
    } = this.props;
    const b = block('data-list-layout');
    let pagesCount;
    if (!isNaN(displayParams.count)) {
      pagesCount = totalItemsCount % displayParams.count === 0 ?
        totalItemsCount / displayParams.count : Math.floor(totalItemsCount / displayParams.count) + 1;
    } else {
      pagesCount = 1;
    }
    const optionsPageLimitLocal = Object.keys(optionsPageLimit).map(key =>
      <option value={optionsPageLimit[key].value}>{optionsPageLimit[key].text}</option>
    );
    return (
      <div className={b}>
        <div className="center-content-block">
          <h3>{ getEntityName(params.entity) }</h3>
        </div>
        <div className={b('top-row')}>
          { settings != null && settings.permissions.includes(PermissionTypes.CREATE) &&
            <div className={b('create-item-block')}>
              <Button onClick={this.onCreateDataItemClick} bsStyle="success">Добавить запись</Button>
            </div>
          }
          <label className={b('page-limit', { label: true })}>
            Выберите количество строк на одной странице:<br />
            <select 
              className={b('page-limit', { input: true })}
              type='number'
              value={this.state.countPage}
              onChange={this.onChangeCountPage}
            >
              {optionsPageLimitLocal}
            </select>
          </label>
        </div>
        <FilterBlock settings={settings} onApplyFilters={this.onApplyFilters} />
        <DataTableView
          onShowFull={this.onShowFullDataItemClick}
          onEdit={this.onDataItemEditClick}
          onRemove={this.onRemoveDataItemClick}
          data={data}
          settings={settings}
          onFieldClick={this.onFieldClick}
          isDataLoading={actionProcessing}
        />
        <div className="center-content-block">
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={pagesCount}
            maxButtons={5}
            activePage={activePage}
            onSelect={this.handlePageSelect}
            className="pagination-component"
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    settings: state.commonData.settings,
    loadSettingsSuccess: state.commonData.loadSettingsSuccess,
    data: state.commonData.data,
    actionProcessing: state.commonData.actionProcessing,
    errorMessage: state.commonData.errorMessage,
    totalItemsCount: state.commonData.totalItemsCount,
    activePage: state.commonData.activePage,
    displayParams: state.commonData.displayParams,
  };
}

function mapDispatchToProps(dispatch) {
  actions.resetFilterValues = filterActions.resetFilterValues;
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CommonDataListLayout);
