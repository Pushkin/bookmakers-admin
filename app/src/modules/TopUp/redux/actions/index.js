const actionTypes = {
  ACTION_PROCESSING: 'wj-admin/topUp/ACTION_PROCESSING',
  ACTION_FAILURE: 'wj-admin/topUp/ACTION_FAILURE',
  TOPUP_SUCCESS: 'wj-admin/topUp/TOPUP_SUCCESS',
  FLUSH_REQUEST_DATA: 'wj-admin/ads/FLUSH_REQUEST_DATA',
};

function topUp(topUpData) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'topUp',
      data: {
        user_id: topUpData.userID,
        amount: topUpData.amount,
        is_bonus: topUpData.is_bonus,
        note: topUpData.note,
      },
    };
    const response = await api.user.topUp(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.TOPUP_SUCCESS });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

function flushRequestData() {
  return { type: actionTypes.FLUSH_REQUEST_DATA };
}

export {
  actionTypes,
  topUp,
  flushRequestData,
};
