import { fromJS } from 'immutable';
import initialState from '../initial';
import { actionTypes } from '../actions';

function reducer(state = initialState, action) {
  const imState = fromJS(state);
  
  switch (action.type) {
    case actionTypes.ACTION_PROCESSING:
      return imState
        .setIn(['actionProcessing'], true)
        .setIn(['errorMessage'], null)
        .setIn(['actionSuccess'], false)
        .toJS();
    case actionTypes.ACTION_FAILURE:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['actionSuccess'], false)
        .setIn(['errorMessage'], action.payload)
        .toJS();
    case actionTypes.TOPUP_SUCCESS:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['actionSuccess'], true)
        .toJS();
    case actionTypes.FLUSH_REQUEST_DATA:
      return imState
        .setIn(['actionSuccess'], false)
        .setIn(['actionProcessing'], false)
        .setIn(['errorMessage'], null)
        .toJS();
    default:
      return imState
        .toJS();
  }
}

export default reducer;
