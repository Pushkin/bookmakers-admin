const initialState = {
  actionProcessing: false,
  errorMessage: null,
  actionSuccess: false,
};

export default initialState;
