import React, { Component, PropTypes } from 'react';
import block from 'bem-cn';
import { bind } from 'decko';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from '../../redux';
import { Button, ControlLabel, FormControl, FormGroup } from 'react-bootstrap';
import FieldGroup from 'shared/view/components/FieldGroup';
import SubmitBlock from 'shared/view/components/SubmitBlock';
import './Layout.styl';
import CheckboxGroup from "../../../../shared/view/components/CheckboxGroup/CheckboxGroup";

class TopUpLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userID: -1,
      amount: 0,
      is_bonus: false,
    };
  }

  static propTypes = {
    topUp: PropTypes.func.isRequired,
    actionProcessing: PropTypes.bool.isRequired,
    errorMessage: PropTypes.bool.isRequired,
    actionSuccess: PropTypes.bool.isRequired,
    flushRequestData: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.flushRequestData();
  }

  @bind
  onInputChange({ currentTarget: { id, value } }) {
    this.setState({ [id]: value });
  }

  @bind
  onTopupClick() {
    this.props.topUp(this.state);
  }

  @bind
  onCheckBoxChange(currentTarget) {
    const key = currentTarget.target.id;
    console.log(`${currentTarget.target.id}: ${!this.state[key]}`);
    this.setState({ [key]: !this.state[key] });
  }

  render() {
    const b = block('top-up-container');
    const { actionProcessing, errorMessage, actionSuccess } = this.props;
    return (
      <div className={b}>
        <h3 className="page-title">Начисление бонусов</h3>
        <div className={b('form')}>
          <FieldGroup
            id="userID"
            label="ID пользователя"
            type="number"
            onChange={this.onInputChange}
          />
          <FieldGroup
            id="amount"
            label="Сумма для начисления"
            type="number"
            onChange={this.onInputChange}
          />
          <CheckboxGroup
            id="is_bonus"
            label="На бонусный счет"
            onChange={this.onCheckBoxChange}
          />
          <FormGroup>
            <ControlLabel className="input-label">Примечание</ControlLabel>
            <FormControl
              id="note"
              componentClass="textarea"
              onChange={this.onInputChange}
            />
          </FormGroup>
          <SubmitBlock
            submitText="Начислить"
            actionProcessing={actionProcessing}
            actionSuccess={actionSuccess}
            errorMessage={errorMessage}
            onSubmitClick={this.onTopupClick}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    actionProcessing: state.topUp.actionProcessing,
    errorMessage: state.topUp.errorMessage,
    actionSuccess: state.topUp.actionSuccess,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TopUpLayout);
