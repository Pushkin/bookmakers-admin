import * as React from 'react';
import { Route } from 'react-router';
import TopUpLayout from './view/Layout';
import { reducer } from './redux';

class TopUpModule {
  getRoutes() {
    return (
      <Route key="topup" path="topup" component={TopUpLayout} />
    );
  }

  getReducer() {
    return { name: 'topUp', reducer };
  }
}

export default TopUpModule;
