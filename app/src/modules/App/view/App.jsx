import React, { Component, PropTypes } from 'react';
import block from 'bem-cn';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'assets/theme.css';
import Header from 'shared/view/components/Header';
import { actions } from '../redux';
import './App.styl';

class App extends Component {
  static propTypes = {
    isUserAuthenticated: PropTypes.bool.isRequired,
    entities: PropTypes.arrayOf(String).isRequired,
    loadEntities: PropTypes.func.isRequired,
    actionProcessing: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
  }

  componentDidMount() {
    this.props.loadEntities();
  }

  render() {
    const b = block('app-container');
    const { isUserAuthenticated, entities } = this.props;
    return (
      <div className={b}>
        <Header entities={entities} isUserAuthenticated={isUserAuthenticated} />
        <div className={b('app-content')}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isUserAuthenticated: state.auth.isAuthenticated,
    entities: state.app.entities,
    actionProcessing: state.app.actionProcessing,
    errorMessage: state.app.errorMessage,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
