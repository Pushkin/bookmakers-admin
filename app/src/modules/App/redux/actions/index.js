const actionTypes = {
  ACTION_PROCESSING: 'wj-admin/app/ACTION_PROCESSING',
  ACTION_SUCCESS: 'wj-admin/app/ACTION_SUCCESS',
  ACTION_FAILURE: 'wj-admin/app/ACTION_FAILURE',
  LOAD_ENTITIES_LIST_SUCCESS: 'wj-admin/app/GET_ENTITIES_LIST',
};

function loadEntities() {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'loadEntities',
    };
    const response = await api.common.loadEntities(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.LOAD_ENTITIES_LIST_SUCCESS, payload: response.data });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

export { 
  actionTypes,
  loadEntities,
};
