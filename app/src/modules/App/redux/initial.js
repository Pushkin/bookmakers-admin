const initialState = {
  actionProcessing: false,
  errorMessage: null,
  entities: [],
};

export default initialState;
