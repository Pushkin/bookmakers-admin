import React, { Component, PropTypes } from 'react';
import block from 'bem-cn';
import { bind } from 'decko';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from '../../redux';
import SubmitBlock from 'shared/view/components/SubmitBlock';
import FieldGroup from 'shared/view/components/FieldGroup';
import './EditLayout.styl';

class TerminalCardsEditLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      card: '',
      phone: '',
    };
  }

  static propTypes = {
    actionProcessing: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    createCardSuccess: PropTypes.bool.isRequired,
    createCard: PropTypes.func.isRequired,
  }

  componentDidMount() {
    console.log('terminal c did mount');
  }

  @bind
  onInputChange({ currentTarget: { id, value } }) {
    this.setState({ [id]: value });
  }

  @bind
  onSubmitClick() {
    const { params, createCard } = this.props;
    if (!params.id) createCard(this.state);
  }

  render() {
    const b = block('terminal-cards-container');
    const { params, actionProcessing, createCardSuccess, errorMessage } = this.props;
    const itemID = params.id;
    const { name, card, phone } = this.state;
    return (
      <div className={b}>
        <h3 className="page-title center-content-block">
          {`Карты терминала: ${itemID ? `Редактирование (ID: ${itemID})` : 'Создание'}`}
        </h3>
        <FieldGroup
          id="name"
          label="Name"
          type="text"
          onChange={this.onInputChange}
          value={name}
        />
        <FieldGroup
          id="card"
          label="Card"
          type="text"
          onChange={this.onInputChange}
          value={card}
        />
        <FieldGroup
          id="phone"
          label="Phone"
          type="text"
          onChange={this.onInputChange}
          value={phone}
        />
        <SubmitBlock
          submitText="Сохранить"
          actionProcessing={actionProcessing}
          actionSuccess={createCardSuccess}
          errorMessage={errorMessage}
          onSubmitClick={this.onSubmitClick}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    actionProcessing: state.terminalCards.actionProcessing,
    errorMessage: state.terminalCards.errorMessage,
    createCardSuccess: state.terminalCards.createCardSuccess,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TerminalCardsEditLayout);
