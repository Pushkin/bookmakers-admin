import { fromJS } from 'immutable';
import initialState from '../initial';
import { actionTypes } from '../actions';

function reducer(state = initialState, action) {
  const imState = fromJS(state);
  
  switch (action.type) {
    case actionTypes.ACTION_PROCESSING:
      return imState
        .setIn(['actionProcessing'], true)
        .setIn(['errorMessage'], null)
        .setIn(['createCardSuccess'], false)
        .toJS();

    case actionTypes.ACTION_FAILURE:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['errorMessage'], action.payload)
        .toJS();

    case actionTypes.CREATE_CARD_SUCCESS:
      return imState
        .setIn(['actionProcessing'], false)
        .setIn(['errorMessage'], null)
        .setIn(['createCardSuccess'], true)
        .toJS();
    
    default:
      return imState
        .toJS();
  }
}

export default reducer;
