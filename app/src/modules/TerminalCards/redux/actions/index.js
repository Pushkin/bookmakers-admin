const actionTypes = {
  ACTION_PROCESSING: 'wj-admin/terminalCards/ACTION_PROCESSING',
  ACTION_FAILURE: 'wj-admin/terminalCards/ACTION_FAILURE',
  CREATE_CARD_SUCCESS: 'wj-admin/terminalCards/CREATE_CARD_SUCCESS',
};

function createCard(data) {
  return async (dispatch, getState, api) => {
    dispatch({ type: actionTypes.ACTION_PROCESSING });
    const requestData = {
      key: 'loadSettings',
      data,
    };
    const response = await api.terminal.createCard(requestData);
    if (response.success) {
      dispatch({ type: actionTypes.CREATE_CARD_SUCCESS, payload: response.data });
    } else dispatch({ type: actionTypes.ACTION_FAILURE, payload: response.errorMessage });
  };
}

export {
  actionTypes,
  createCard,
};
