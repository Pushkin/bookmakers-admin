const initialState = {
  actionProcessing: false,
  errorMessage: null,
  createCardSuccess: false,
};

export default initialState;
