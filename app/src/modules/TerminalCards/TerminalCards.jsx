import * as React from 'react';
import { Route } from 'react-router';
import TerminalCardsEditLayout from './view/EditLayout';
import { reducer } from './redux';

class TerminalCardsModule {
  getRoutes() {
    return (
      <Route key="terminalCards" path="terminal_cards" >
        <Route path="create" component={TerminalCardsEditLayout} />
        <Route path="edit/:id" component={TerminalCardsEditLayout} />
      </Route>
    );
  }

  getReducer() {
    return { name: 'terminalCards', reducer };
  }
}

export default TerminalCardsModule;
