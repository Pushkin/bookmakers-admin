import React from 'react';
import { ControlLabel, FormGroup, Checkbox } from 'react-bootstrap';
import './CheckboxGroup.styl';

function CheckboxGroup({ id, label, checked, ...props}) {
  return (
    <FormGroup controlId={id}>
      <ControlLabel className="input-label">{label}</ControlLabel>
      <Checkbox id={id} checked={checked} {...props} />
    </FormGroup>
  );
}

export default CheckboxGroup;
