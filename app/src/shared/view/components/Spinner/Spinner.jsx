import React, { PropTypes } from 'react';
import Loader from 'halogen/ClipLoader';
import block from 'bem-cn'; 

import './Spinner.styl';

class Spinner extends React.Component {
  static propTypes ={
    isLoading: PropTypes.bool.isRequired,
  }
  
  render() {
    const b = block('spinner-wrapper');
    const { isLoading } = this.props;
    return (
      <div className={b}>
        <div className={b('spinner')}>
          <Loader loading={+isLoading} color="#26A65B" size="8rem" />
        </div>
        <div className={b('background', { isLoading })}></div>
      </div>
    );
  }
}
export default Spinner;
