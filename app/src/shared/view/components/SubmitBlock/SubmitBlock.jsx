import React, { Component, PropTypes } from 'react';
import block from 'bem-cn';
import { bind } from 'decko';
import './SubmitBlock.styl';
import { Button } from 'react-bootstrap';

class SubmitBlock extends Component {
  static propTypes = {
    submitText: PropTypes.string.isRequired,
    actionProcessing: PropTypes.bool.isRequired,
    actionSuccess: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string.isRequired,
    onSubmitClick: PropTypes.func.isRequired,
    validationOK: PropTypes.bool,
  }

  static defaultProps = {
    validationOK: true,
  }
  
  @bind
  handleSubmitClick() {
    const { validationOK, actionProcessing, onSubmitClick } = this.props;
    if (validationOK && !actionProcessing) onSubmitClick();
  }

  render() {
    const { errorMessage, actionProcessing, actionSuccess,
       submitText, validationOK } = this.props;
    const b = block('submit-block');
    return (
      <div className={b}>
        { errorMessage &&
          <div className={b('error-block')}>
            {errorMessage}
          </div>
        }
        { actionSuccess &&
          <div className={b('success-block')}>
            Операция выполнена успешно
          </div>
        }
        <div className={b('button-block')}>
          <Button
            disabled={actionProcessing || !validationOK}
            bsStyle="success"
            onClick={this.handleSubmitClick}
          >
            {submitText}
          </Button>
        </div>
      </div>
    );
  }
}

export default SubmitBlock;
