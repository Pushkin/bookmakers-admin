import React from 'react';
import DatePicker from 'react-bootstrap-date-picker';
import { ControlLabel, FormGroup } from 'react-bootstrap';
import './DatePickerGroup.styl';

function DatePickerGroup({ id, label, ...props}) {
  return (
    <FormGroup controlId={id}>
      <ControlLabel className="input-label">{label}</ControlLabel>
      <DatePicker {...props} />
    </FormGroup>
  );
}

export default DatePickerGroup;
