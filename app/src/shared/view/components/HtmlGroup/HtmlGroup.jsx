import React, { Component, PropTypes } from 'react';
import { ControlLabel, FormGroup, FormControl, Tabs, Tab } from 'react-bootstrap';
import ReactQuill from 'react-quill';
import { bind } from 'decko';
import pretty from 'pretty';
import 'react-quill/dist/quill.snow.css';
import './HtmlGroup.styl';

const tabs = {
  TAB_EDITOR: 1,
  TAB_HTML: 2,
};

class HtmlGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: tabs.TAB_EDITOR,
      html: '',
      textHtml: '',
      modules: {
        toolbar: [
          [{ font: [] }, { size: [] }],
          [{ align: [] }, 'direction'],
          ['bold', 'italic', 'underline', 'strike'],
          [{ color: [] }, { background: [] }],
          [{ script: 'super' }, { script: 'sub' }],
          ['blockquote', 'code-block'],
          [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
          ['link', 'image', 'video'],
          ['clean'],
        ],
      },
    };
  }

  static propTypes = {
    id: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
  }

  static defaultProps = {
    onChange: () => {},
  }

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (this.props.value !== value) {
      this.setState({ html: value });
    }
  } 

  @bind
  handleSelectTab(tab) {
    this.setState({ activeTab: tab });
  }

  @bind
  onHTMLEditorChange(html) {
    this.setState({ html, textHtml: pretty(html) });
    this.props.onChange(html);
  }

  @bind
  onTextHTMLEditorChange({ currentTarget: { id, value } }) {
    this.setState({ [id]: value });
  }

  @bind
  onTextHTMLBlur({ currentTarget: { id, value } }) {
    this.setState({ html: value });
    this.props.onChange(value);
  }

  render() {
    const { id, label, value, ...props } = this.props;
    const { modules, textHtml, html } = this.state;
    return (
      <FormGroup controlId={id}>
        <ControlLabel className="input-label">{label}</ControlLabel>
        <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab-example">
          <Tab eventKey={tabs.TAB_EDITOR} title="Редактор">
            <ReactQuill
              theme="snow"
              onChange={this.onHTMLEditorChange}
              modules={modules}
              value={html}
            />
          </Tab>
          <Tab eventKey={tabs.TAB_HTML} title="HTML">
            <FormGroup className="html-textarea-group">
              <FormControl
                id="textHtml"
                className="html-textarea"
                componentClass="textarea"
                onBlur={this.onTextHTMLBlur}
                onChange={this.onTextHTMLEditorChange}
                value={textHtml}
              />
            </FormGroup>
          </Tab>
        </Tabs>
      </FormGroup>
    );
  }
}

export default HtmlGroup;
