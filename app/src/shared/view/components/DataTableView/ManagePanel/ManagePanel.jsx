import React, { Component, PropTypes } from 'react';
import TableManageButton from 'shared/view/elements/TableManageButton';
import block from 'bem-cn';
import { PermissionTypes } from 'shared/utils/data';
import './ManagePanel.styl';

class ManagePanel extends Component {
  static propTypes = {
    onShowFull: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    permissions: PropTypes.arrayOf(String).isRequired,
  } 
  
  render() {
    const b = block('table-manage-panel');
    const { onEdit, onRemove, onShowFull, permissions } = this.props;
    return (
      <div className={b}>
        { permissions.includes(PermissionTypes.READ) &&
          <TableManageButton onClick={onShowFull} glyphName="list" />
        }
        { permissions.includes(PermissionTypes.UPDATE) &&
          <TableManageButton onClick={onEdit} glyphName="pencil" />
        }
        { permissions.includes(PermissionTypes.DELETE) &&
          <TableManageButton onClick={onRemove} glyphClassName="manage-button-remove" glyphName="remove" />
        }
      </div>
    );
  }
}

export default ManagePanel;
