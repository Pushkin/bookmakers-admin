import React, { Component, PropTypes } from 'react';
import { bind } from 'decko';
import './DataTableRow.styl';
import ManagePanel from '../ManagePanel';
import { convertDate } from 'shared/utils/data';

class DataTableRow extends Component {
  static propTypes = {
    fieldsConfig: PropTypes.object.isRequired,
    headers: PropTypes.arrayOf(String).isRequired,
    dataSet: PropTypes.object.isRequired,
    onEdit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    onShowFull: PropTypes.func.isRequired,
    permissions: PropTypes.arrayOf(String).isRequired,
  }

  @bind
  handleRemoveClick() {
    const { dataSet, onRemove } = this.props;
    onRemove(dataSet.id);
  }

  @bind
  handleEditClick() {
    const { dataSet, onEdit } = this.props;
    onEdit(dataSet.id);
  }

  @bind
  handleShowFullClick() {
    const { dataSet, onShowFull } = this.props;
    onShowFull(dataSet.id);
  }

  render() {
    const { dataSet, headers, permissions, fieldsConfig } = this.props;
    const dataList = headers.map((header) => {
      switch (fieldsConfig[header.key].type) {
        case 'date':
          return <td key={header.key}>{ dataSet[header.key] ? convertDate(dataSet[header.key]) : '' }</td>;
        case 'enum':
          const enumElem = fieldsConfig[header.key].enum[dataSet[header.key]] ?
            fieldsConfig[header.key].enum[dataSet[header.key]] : '';

          return <td key={header.key}>{ enumElem }</td>;
        case 'boolean':
          return <td key={header.key}>{typeof dataSet[header.key] ? '' + dataSet[header.key] : '' }</td>;
        default:
          return <td key={header.key}>{ dataSet[header.key] ? dataSet[header.key] : '' }</td>;
      }
    });
    dataList.push(
      <td key="manage" >
        <ManagePanel
          permissions={permissions}
          onShowFull={this.handleShowFullClick}
          onEdit={this.handleEditClick}
          onRemove={this.handleRemoveClick}
        />
      </td>,
    );
    return (
      <tr>
        { dataList }
      </tr>
    );
  }
}

export default DataTableRow;
