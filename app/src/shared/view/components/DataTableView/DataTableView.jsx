import React, { Component, PropTypes } from 'react';
import { Table } from 'react-bootstrap';
import block from 'bem-cn';
import { bind } from 'decko';
import flatten from 'flat';
import DataTableHeader from './DataTableHeader';
import DataTableRow from './DataTableRow';
import Spinner from 'shared/view/components/Spinner';
import './DataTableView.styl';

class DataTable extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(Object).isRequired,
    settings: PropTypes.shape({
      fields: PropTypes.object,
      permissions: PropTypes.arrayOf(Object),
    }).isRequired,
    onShowFull: PropTypes.func,
    onEdit: PropTypes.func,
    onRemove: PropTypes.func,
    onFieldClick: PropTypes.func,
    isDataLoading: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    onCreate: () => {},
    onShowFull: () => {},
    onEdit: () => {},
    onRemove: () => {},
    onFieldClick: (header) => { console.log(`header click: ${header}`); },
  }

  @bind
  getTableHeadersFromFields(fields) {
    const { first, rest } = Object.keys(fields)
      .filter(key => fields[key].table)
      .reduce(
        ({ first: f, rest: r }, cur) => {
          const newElem = { key: cur, name: fields[cur].name ? fields[cur].name : cur };
          return cur === 'id' ?
          ({ first: [newElem], rest: r })
          :
          ({ first: f, rest: [...r, newElem] });
        },
        { first: [], rest: [] },
      );
    return first.concat(rest);
  }

  render() {
    const b = block('data-table-view');
    const { 
      data,
      onShowFull,
      onEdit,
      onRemove,
      settings,
      onFieldClick,
      isDataLoading,
    } = this.props;
    const headers = settings ? this.getTableHeadersFromFields(settings.fields) : [];
    const tableRows = data.map((dataSet, index) => (
      <DataTableRow
        onShowFull={onShowFull}
        onEdit={onEdit}
        onRemove={onRemove}
        key={`data-row-${index}`}
        dataSet={flatten(dataSet)}
        headers={headers}
        fieldsConfig={settings ? settings.fields : {}}
        permissions={settings.permissions}
      />
    ));
    return (
      <div className={b} >
        <div>
          <Spinner isLoading={isDataLoading} />
          <Table striped bordered condensed hover>
            <DataTableHeader onFieldClick={onFieldClick} headers={headers} />
            <tbody>
              {tableRows}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default DataTable;
