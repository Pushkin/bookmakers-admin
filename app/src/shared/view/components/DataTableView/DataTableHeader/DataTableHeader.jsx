import { bind } from 'decko';
import React, { Component, PropTypes } from 'react';
import './DataTableHeader.styl';

class DataTableHeader extends Component {
  static propTypes = {
    headers: PropTypes.arrayOf(String).isRequired,
    onFieldClick: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      keys: {},
    };
  }

  @bind
  reverseKey(key) {
    let newKey;
    if (key[0] === '-') {
      newKey = key.slice(1);
    }
    else {
      newKey = '-' + key;
    }
    return newKey;
  }

  @bind
  fieldClick(inKey) {
    const { headers } = this.props;
    const { keys } = this.state;
    const index = keys.findIndex((el, index) => {
      return el === inKey || el.slice(1) === inKey;
    });
    const newKey = this.reverseKey(keys[index]);
    const keyList = keys;
    keyList[index] = newKey;
    this.setState({ keys: keyList });

    this.props.onFieldClick(newKey);
  }

  @bind
  setKeys(headers) {
    const keyList = headers.map(header => (header.key));
    this.setState({ keys: keyList });
  }

  componentWillReceiveProps() {
    const { headers } = this.props;
    const { keys } = this.state;
    if (!keys[0] && headers[0]) {
      const index = headers.findIndex((el, index) => {
        return el.key === 'id';
      });
      if (index >= 0) {
        headers[index].key = '-id';
      }
      this.setKeys(headers);
    }
  }

  render() {
    const { headers, onFieldClick } = this.props;
    const headersList = headers.map(header => (
      <th
        onClick={() => this.fieldClick(header.key)}
        className="data-table-header-item"
      >
        {header.name}
      </th>
      ),
    );
    return (
      <thead>
        <tr>
          { headersList }
          <th>MANAGE</th>
        </tr>
      </thead>
    );
  }
}

export default DataTableHeader;
