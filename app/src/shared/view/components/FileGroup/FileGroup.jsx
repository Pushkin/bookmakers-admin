import React from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

function FileGroup({ id, label, help, ...props }) {
  return (
    <FormGroup controlId={id}>
      { label && <ControlLabel className="input-label">{label}</ControlLabel> }
      <FormControl type="file" {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}

export default FileGroup;