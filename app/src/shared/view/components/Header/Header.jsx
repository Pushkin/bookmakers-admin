import React, { Component, PropTypes } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import { browserHistory, Link } from 'react-router';
import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
} from 'react-bootstrap';
import { getEntityName } from 'shared/utils/data';
import AuthWrapper from 'modules/shared/AuthWrapper';
import './Header.styl';

class Header extends Component {
  static propTypes = {
    isUserAuthenticated: PropTypes.bool.isRequired,
    entities: PropTypes.arrayOf(String).isRequired,
  }

  render() {
    const { isUserAuthenticated, entities } = this.props;
    const entityLinkList = entities.map((entity, index) => {
      return (
        <LinkContainer to={`/admin/${entity}/list`}>
          <NavItem eventKey={index + 3}>{getEntityName(entity)}</NavItem>
        </LinkContainer>
      );
    });
    return (
      <Navbar inverse collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">Admin</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
          <div id="google_translate_element"></div>
        </Navbar.Header>
        <Navbar.Collapse>
          { isUserAuthenticated && 
            <Nav>
              <LinkContainer to="/admin/topup">
                <NavItem eventKey={1}>Начисление средств</NavItem>
              </LinkContainer>
              {/*<LinkContainer to="/admin/terminal_cards">*/}
                {/*<NavItem eventKey={2}>Карты терминала</NavItem>*/}
              {/*</LinkContainer>*/}
              {entityLinkList}
            </Nav>
          }
          <Nav pullRight>
            <AuthWrapper />
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Header;
