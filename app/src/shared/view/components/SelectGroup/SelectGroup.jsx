import React, { Component, PropTypes } from 'react';
import { ControlLabel, FormGroup, FormControl } from 'react-bootstrap';
import './SelectGroup.styl';

class SelectGroup extends Component {
  static defaultProps = {
    onChange: () => {},
  }

  componentDidMount() {
    const { value, itemsEnum, onChange, id } = this.props;
    if (typeof value === 'undefined' || value == null || value === '') {
      const firstItemValue = Object.keys(itemsEnum)[0];
      console.log(firstItemValue)
      onChange({ currentTarget: { id, value: firstItemValue } });
    }
  }

  render() {
    const { itemsEnum, filtering, id, label, ...props} = this.props;
    const itemsList = Object.keys(itemsEnum)
      .map(key => <option value={key} >{itemsEnum[key]}</option>);
    return (
      <FormGroup controlId={id}>
        <ControlLabel className="input-label">{label}</ControlLabel>
        <FormControl {...props} componentClass="select" placeholder="select">
          { filtering && <option value="">Не выбрано</option> }
          {itemsList}
        </FormControl>
      </FormGroup>
    );
  }
}

export default SelectGroup;
