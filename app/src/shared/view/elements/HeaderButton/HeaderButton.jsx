import React, { Component, PropTypes } from 'react';
import block from 'bem-cn';
import { Glyphicon } from 'react-bootstrap';
import './HeaderButton.styl';

class HeaderButton extends Component {
  static propTypes = {
    glyphName: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    glyphName: '',
    text: null,
    onClick: () => {},
  }

  render() {
    const b = block('header-button');
    const { glyphName, text, onClick } = this.props;
    return (
      <button onClick={onClick} className={b}>
        <Glyphicon glyph={glyphName} />
        { text && 
          <span className={b('text')}> { text } </span>
        }
      </button>
    );
  }
}

export default HeaderButton;
