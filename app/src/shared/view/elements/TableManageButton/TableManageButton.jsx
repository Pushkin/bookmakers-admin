import React, { Component, PropTypes } from 'react';
import { Glyphicon } from 'react-bootstrap';
import block from 'bem-cn';
import './TableManageButton.styl';

class TableManageButton extends Component {
  static propTypes = {
    glyphName: PropTypes.string,
    onClick: PropTypes.func,
    glyphClassName: PropTypes.string,
  }

  static defaultProps = {
    glyphName: '',
    onClick: () => {},
    clglyphClassNameassName: '',
  }

  render() {
    const b = block('table-manage-button');
    const { glyphName, onClick, glyphClassName } = this.props;
    return (
      <button onClick={onClick} className={b}>
        <Glyphicon className={glyphClassName} glyph={glyphName} />
      </button>
    );
  }
}

export default TableManageButton;
