const PermissionTypes = {
  CREATE: 'CREATE',
  READ: 'GET',
  UPDATE: 'UPDATE',
  DELETE: 'DELETE',
};

const _entitiesNames = {
  flat_page: 'Простые страницы',
  user: 'Пользователи',
  feedback: 'Обратная связь',
  ads: 'Реклама',
  transfers: 'Трансферы',
  currency_rate: 'Курсы Валют',
  city: 'Города',
  withdrawal: 'Вывод средств',
  promo_code: 'Промо коды',
  slide: 'Слайдер',
  email_notification: 'Почтовые уведомления',
  inbet_session: 'Сессия Inbet',
  bonus_settings: 'Настройки бонусов',
  bonus_transfers: 'Бонусные трансферы',
  partner_list: 'Список партнеров',
  partner_transactions: 'Транзакции партнера',
  partner_withdrawals: 'Выводы средств партнера',
  settings: 'Настройки',
  bets: 'Ставки',
  categories: 'Категории',
  promo_materials: 'промо материалы',
};

function getEntityName(entityKey) {
  const entityName = _entitiesNames[entityKey];
  return entityName ? entityName : entityKey;
}

function convertDate(string) {
  if (string == null) return '';
  const date = new Date(string);
  const offset = date.getTimezoneOffset();
  if (offset > 0) {
    date.setTime(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
  }
  else if (offset < 0) {
    date.setTime(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
  }
  const dateOptions = {
    year: 'numeric', month: 'long', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric',
  };
  return date.toLocaleDateString('ru', dateOptions);
}

export {
  PermissionTypes,
  getEntityName,
  convertDate,
};

