import { bind } from 'decko';

class UserApi {
  constructor(sendRequest, getToken) {
    this.sendRequest = sendRequest;
    this.getToken = getToken;
  }

  @bind
  async topUp(requestData) {
    const socketData = {
      method: 'POST',
      url: '/user/bonus/add/',
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
      data: requestData.data,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

}

export default UserApi;
