import { bind } from 'decko';

class TerminalApi {
  constructor(sendRequest, getToken) {
    this.sendRequest = sendRequest;
    this.getToken = getToken;
  }

  @bind
  async createCard(requestData) {
    const socketData = {
      method: 'POST',
      url: '/user/card/register',
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
      data: requestData.data,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

}

export default TerminalApi;
