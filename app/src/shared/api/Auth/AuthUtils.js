const errorMessages = {
  1: 'Ошибка авторизации',
};

class AuthUtils {
  static getErrorMessage(code) {
    return errorMessages[code];
  }
}

export default AuthUtils;
