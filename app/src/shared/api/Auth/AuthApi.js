import { bind } from 'decko';
import AuthUtils from './AuthUtils';

class AuthApi {
  constructor(sendRequest, getToken) {
    this.sendRequest = sendRequest;
    this.getToken = getToken;
  }

  @bind
  updateResponseWithErrorMessage(response) {
    if (!response.success) {
      response.errorMessage = AuthUtils.getErrorMessage(response.errorCode);
    }
    return response;
  }

  @bind
  async signIn(requestData) {
    const socketData = {
      method: 'POST',
      url: '/login/',
      data: requestData.payload,
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return this.updateResponseWithErrorMessage(response);
  }

  @bind
  async checkAuth(requestData) {
    const socketData = {
      method: 'GET',
      url: '/check/',
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }
}

export default AuthApi;
