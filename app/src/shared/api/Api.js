import { bind } from 'decko';
import AuthApi from './Auth/AuthApi';
import UserApi from './UserApi';
import AdsApi from './AdsApi';
import CommonDataApi from './CommonDataApi';
import TerminalApi from './TerminalApi';

class Api {
  constructor(baseHost, onCreatedCallback) {
    this.baseHost = baseHost;
    this.socket = this.configureSocket(baseHost);
    this.socket.onopen = onCreatedCallback;
    this.activeRequests = [];
    const token = localStorage.getItem('userToken');
    this.token = token ? token : '';

    this.auth = new AuthApi(this.sendRequest, this.getUserToken);
    this.user = new UserApi(this.sendRequest, this.getUserToken);
    this.ads = new AdsApi(this.sendRequest, this.getUserToken);
    this.common = new CommonDataApi(this.sendRequest, this.getUserToken);
    this.terminal = new TerminalApi(this.sendRequest, this.getUserToken);
  }

  @bind
  configureSocket(baseHost) {
    const socket = new WebSocket(baseHost);
    socket.onmessage = this.onSocketMessage;
    socket.onerror = this.onSocketError;
    socket.onclose = () => {
      setTimeout(() => { this.socket = this.configureSocket(baseHost); }, 3000);
    };
    return socket;
  }

  @bind
  getUserToken() {
    return this.token;
  }

  @bind
  setUserToken(token) {
    this.token = token;
    localStorage.setItem('userToken', token);
  }

  @bind
  resetUserToken() {
    this.userToken = '';
    localStorage.removeItem('userToken');
  }

  @bind
  onSocketMessage(e) {
    const requestData = JSON.parse(e.data);
    const eventName = requestData.event_name;
    const eventData = requestData.data;
    const success = eventData.status === 0;
    const requestIndex = this.activeRequests.findIndex(request => request.key === eventName);
    const request = this.activeRequests[requestIndex];
    
    if (request) {
      const responseData = {
        success,
        data: success ? eventData.result : null,
        errorMessage: success ? null : 'Ошибка выполнения запроса', // !!!!!!!!!!!
        errorCode: success ? null : eventData.status,
      };
      request.callback(responseData);
    } else console.log(`unable to find active request with key '${eventName}'`);
    this.activeRequests = [
      ...this.activeRequests.slice(0, requestIndex),
      ...this.activeRequests.slice(requestIndex + 1),
    ];
  }

  @bind
  onSocketError() {

  }

  @bind
  sendRequest(requestData, socketData) {
    return new Promise((resolve, reject) => {
      requestData.callback = response => resolve(response);
      this.activeRequests.push(requestData);
      this.socket.send(JSON.stringify(socketData));
    });
  }

}

export default Api;
