import { bind } from 'decko';

class CommonDataApi {
  constructor(sendRequest, getToken) {
    this.sendRequest = sendRequest;
    this.getToken = getToken;
  }

  @bind
  async loadEntities(requestData) {
    const socketData = {
      method: 'GET',
      url: '/entity/',
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async loadSettings(requestData) {
    const socketData = {
      method: 'GET',
      url: `/${requestData.params.entityName}/settings/`,
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async loadData(requestData) {
    const socketData = {
      method: 'GET',
      url: `/${requestData.params.entityName}/list/`,
      args: {
        token: this.getToken(),
        ...requestData.params.args,
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async createDataItem(requestData) {
    const socketData = {
      method: 'POST',
      url: `/${requestData.params.entityName}/create/`,
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
      data: requestData.data,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async removeDataItem(requestData) {
    const socketData = {
      method: 'DELETE',
      url: `/${requestData.params.entityName}/${requestData.params.itemID}/`,
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async loadDataItem(requestData) {
    const socketData = {
      method: 'GET',
      url: `/${requestData.params.entityName}/${requestData.params.itemID}/`,
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async editDataItem(requestData) {
    const socketData = {
      method: 'PUT',
      url: `/${requestData.params.entityName}/${requestData.params.itemID}/`,
      args: {
        token: this.getToken(),
      },
      data: requestData.data,
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async uploadImage(requestData) {
    const socketData = {
      method: 'POST',
      url: '/upload/image/',
      args: {
        token: this.getToken(),
      },
      data: requestData.data,
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }
}

export default CommonDataApi;
