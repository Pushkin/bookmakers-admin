import { bind } from 'decko';

class AuthApi {
  constructor(sendRequest, getToken) {
    this.sendRequest = sendRequest;
    this.getToken = getToken;
  }

  @bind
  async loadAds(requestData) {
    const socketData = {
      method: 'GET',
      url: '/ads/list/',
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async createNewAd(requestData) {
    const socketData = {
      method: 'POST',
      url: '/ads/create/',
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
      data: requestData.data,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async removeAd(requestData) {
    const socketData = {
      method: 'DELETE',
      url: `/ads/${requestData.data}/`,
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async loadAd(requestData) {
    const socketData = {
      method: 'GET',
      url: `/ads/${requestData.data}/`,
      args: {
        token: this.getToken(),
      },
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }

  @bind
  async saveExistingAd(requestData) {
    const socketData = {
      method: 'PUT',
      url: `/ads/${requestData.params.id}/`,
      args: {
        token: this.getToken(),
      },
      data: requestData.data,
      event_name: requestData.key,
    };
    const response = await this.sendRequest(requestData, socketData);
    return response;
  }
}

export default AuthApi;
