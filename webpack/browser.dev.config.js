const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
 
module.exports = {
  context: path.resolve(__dirname, '..', 'app'),
  entry: './src/index.jsx',
  output: {
    path: path.resolve(__dirname, '..', 'app', 'web'),
    filename: 'bundle.js',
    publicPath: '/',
    library: 'WebApp',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015', 'react', 'stage-0'],
              plugins: [
                'transform-decorators-legacy',
                'transform-runtime',
                'transform-regenerator',
                'transform-async-to-generator',
              ],
            },
          },
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.html/,
        use: 'html-loader',
      },
      {
        test: /\.styl/,
        use: [
          'style-loader',
          'css-loader',
          'stylus-loader',
        ],
      },
      {
        test: /\.css/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.(otf|svg|ttf|eot|woff(2)?)(\?[a-z0-9]+)?$/,
        use: 'file-loader?name=fonts/[hash].[ext]',
        exclude: /(app\/modules\/common\/images)/,
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.ENV': JSON.stringify(process.env.ENV || 'browser'),
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/assets/index.html',
      inject: 'head',
    }),
  ],
  resolve: {
    modules: ['node_modules', path.resolve(__dirname, '..', 'app', 'src')],
    extensions: ['.js', '.json', '.jsx', '.css'],
  },
  devServer: {
    contentBase: path.resolve(__dirname, '..', 'app', 'web'),
    inline: true,
    port: 8001,
    historyApiFallback: true,
  },
};
